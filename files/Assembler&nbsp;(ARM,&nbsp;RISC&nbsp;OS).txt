; Hello World in ARM code assembler, with RISC OS software interrupt

SWI "OS_WriteS"
EQUS "Hello World!"
EQUB 0
ALIGN
MOV PC,R14
