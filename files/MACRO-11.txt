;       "Hello, world!" in MACRO-11 for RT-11

        .MCALL  .EXIT,.PRINT
START:  .PRINT  #$1
        .EXIT
$1:     .ASCIZ  /Hello, world!/
        .END    START
