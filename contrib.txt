Reinhard Hamann: Prolog, Lisp, 68000 Assembler (Atari)
Malte Schiphorst, Gino Lucrezi: Pascal
Stefan Radermacher: TeX
Marcus Schmidke: Occam, MSDOS batch, Intel Assembler, 6502 Assembler (C64)
Ralf Lenz: Fortran IV, VMS DCL
Marc Van-Woerkom: C++
Thomas Menschel: 68008 Assembler
Heribert Otten: Cobol, PL/1
Thomas Piske: BS2000 batch
Thomas Preymesser: Pocket calculator
Adrian Nye & Tim O'Reilly: X11-Athena
Udo Halfmann: ELAN
Pedro Martin, Rodrigo Cacilhas: Portuguese
Stefan Llabres: Postscript
Volker Wurst: Smalltalk
Dirk Theisen: Oberon (standard OS)
Georg Bauer: Rexx (simple version), Erlang, SML, Snobol, Setl2, CAML light
Michael Glaesner: Perl, awk
Peter Gummer: Eiffel
Markus Omers: Presentation Manager
Thomas Dorner: SPL4
Jochen Kunz: Rexx (window version), HP-48
Sascha Bendinger: Dylan
Steffen Pohlen: Visual Basic, Gofer (dialog)
Andreas Dierks: Algol-60, Algol-68, Fortran, Fortran-77, dBase IV, Logo
Klaus Mueller: TSO CLIST, OPL
Jens Kilian: Self
Martin Uebing: Intuition
Andreas Maeder: Borland Pascal 7
Stefan Brozinski: Windows, HP-41C
Jens Schaefers: Beta
Wolfgang Houben: PDP-11 Assembler
Pascal Costanza: Oberon (Oberon OS)
Martin Oefelein: Sather, GynkoSoft
Ralf Unland, Claudio Larini: TI-59
Dan Sanders: English translation of TI-59 program
Stefan Rupp: Java
Werner Huber: Informix 4GL
Lutz Heitmann: Turing machine
Federico Hernandez-P&uuml;schel: HTML
Gunter Baumann: NewtonScript
Torsten Landschoff: ST-Guide
Florian Erhard: Gofer (simple)
Niels 'Frolic' Froehling: 68000 Assembler (Amiga)
Michael Sievert: Rexx
Andy Newman: ici
Sammy Mitchell: SAL
Ian Trudel: Icon, Objective C
Chris Locke: Limbo
Victor Koenders: BrainF***
Bora Bali: Assembler IBM 370, IBM Exec, IBM Exec2, ASP (VB-Script and JavaScript), Java Servlet, Turkish, Italian
Neil L. Burman: VAX Macro
Yushin Washio: XHTML
Roel Adriaans: PHP
Ad Boerma: TI-8x/9x, PHP
Paulo Tanimoto: Haskell
Monwar: Bengali
Wolfgang Teschner: Python
Joel Dimbernat: Rebol-view
Todd Nathan: SenseTalk
Heiko Berges: BCPL, ABAP/4, Focal, B, MACRO10
Carlo Keil: Profan
Bruno Bord: French
Andreas Meinl: C++ (Qt)
Mikael Brandstr&ouml;m: Pike
Mariano Corsunsky: Spanish
Larisa Tkachenko, Leandro: PL/SQL
Thomas Fromm: Java Server Pages
Josha Munnik: Shakespeare, Java-Mobile
Fredrik Sandebert: REALbasic
NYYRIKKI: Z80 Assembler for consoles
Simen Schweder: Norwegian
Jeroen Vandezande, Christian Petri, Aux: Delphi
Rodrigo Missiaggia: lua
Dr. Markus Mohnen (c't 25/2003, p. 243): Clean
Egil Groenn: Simula, Norwegian
Nina Cording: Actionscript, Lingo
Stuart Soffer: Mumps
Petri Heikkonen: Whitespace
Marco Pontello: Redcode
Gunnar Jutelius: AXEL
James Robinson: BAL
Krzysztof Szymiczek: Logo (graphical)
Ralf Steines: Amiga-E
Arpadffy Zoltan: OpenVMS DCL
Karlheinz Kronauer: Natural
David Rutter : Spiral
Sanath Kumar.S: Verilog
Jonas Braathen (command line version): mIRC
David Clesse: POV-Ray
Peter Hans van den Muijzenberg: Dutch, Frisian, ZX81-Assembler
Gilson do Nascimento D'Elrei, Rafael Sartori (fixed): Clipper
Marcos Diez: Octave
Mathias P. Gr&auml;dler: PureBasic
Alan Eliasen: Frink
Thomas Marcks von W&uuml;rtemberg: Q-Basic
Pan Yongzhi: Shell scripts
Tynan Sylvester: UnrealScript
Christian Klauser: Command Script
Eric Gauvin: DynaMorph
Daniel Monteiro: Brazilian Portuguese
Tom Demuyt: Judoscript
Pawel Dobrzycki: bc
David Peterson: SIMPLE (submitted by Barry Hitchings)
curian: MSIL
BENI Andras: VRML
Fatty fatty: SQL
Deepak D: Progress
Paul Tingle: POP-11
Istvan Szapudi: OCaml
Jason H. Jester: ratfor
Jason: SQL (Advantage)
Steve Gwilt: TAL
Hynek (Pichi) Vychodil: dc
Petr Adamek: Java (Swing GUI)
David Howell: XSL-FO
Robert McNally: Mathematica
Libor Tvrdik: XSLT
Andrej Krivulcik (The Fox): Slovak
Wikipedia (submitted by Petr Simek): Inform
Jashin Eugene: Windows Scripting Host
A.Shulga: C++ Epoc
Lokshin Mark: PowerScript
Andrey Grigoriev: 1C:Enterprise
Roman Truschev: RSL
Alex Badarin: Gentee (normal and simple version)
Laurent Tonnelier: SVG
Sascha Welter: AppleScript
Aaron Duley: LabVIEW
Andrey V. Sokolov: Russian
Dennis Melentyev: Ukrainian
George D: MIPS Assembler, Visual FoxPro
Shawn Lucas: G Code
Sandor van Voorst: TACL
Stephen Stainless: TI BASIC, TI Extended BASIC
Joan Puigcerver: Catalan
Eugene Ivanov: Win32 Assembler
Darren Debono: VP Assembler
simXa: MAMASH, Hebrew
Tom Kaitchuck: Nice
Jozef Knepp: DML
Per "Muusu" Lindstrand: MATLAB, MDP, QuakeC
Larry Burton: Forth
Doug Gwyn: troff, MACRO-11
John Bito: TECO
John Park: MEL
Yutaka Kachi, Hiroe Shibata, Dario: Japanese
Marcelo Toledo: Emacs Lisp
josh.go: Regular expression
Ric Ford: HyperTalk
Erik Hansen: Cocoa Obj-C
Wesley Cabral: Dataflex Procedural
Klaus Bailly: Finnish
Steven Fisher: Prograph
Steve Ardis: Groovy
Francisco Fernandez: IDC, ASP-VBE
Lukas Jaun: Assembler (MMIX)
Hemant Dabral: PDF, RealText, SMIL
Vasil Bachvarov: Bulgarian
Alexander Toresson: Euphoria
flyingboz: AutoIT3
Jose Taonga: Malagasy
Olivier "oliv3" Girondel: Erlang (fixed)
Grog Grueslayer: Darkbasic
Toby Thain: VAX-11 Macro, Assembler (PDP-8), Assembler (DG Nova)
E.S. van der Meer: Elliott Autocode
Morten Wegelbye Nissen: Danish
Marian Bonda: qore
Zeal Li: Chinese
Farid Effendiyev: Azeri
Morten Lehrman: Ingres ABF
Vadim Konovalov: PL/1 (fixed)
Nikolay Malakhov: sqlplus
Seth Johnson: ColdFusion
Alexander Bogdanov: REFAL-2, MuLisp
Michael Boyarsky: Console Postscript
Vassili Goussakov: Macromedia Flex
Chris Leung: SilverBasic
Iker Mancenido: Basque/Euskara
Zoltan Arpadffy: Hungarian, Serbian, Bosnian, Croatian
Randy Sugianto: Indonesian
Clint Dalrymple: Fj&ouml;lnir
Michail Michailidis: Greek
Marat Buharov: C/AL
Juan Carlos Castro y Castro: Asterisk
Markus Schmaus: Ruby
Henrik Bengtsson: R, S-Plus
Jeffrey Creem: Ada
Nathan Keir: LSL
Michal Nowakowski: Polish
Marko Meza: Slovenian
Vasile V. Alaiba: Romanian
Lorenz Schallinger: Visual Basic .NET
Jack Doerner: Liberty BASIC
Diptesh Ghosh: LaTeX, Maple
Thomas Mertes: Seed7
Sam Birch: SApp
Cindi Knox: mIRC (script and alias versions)
Thomas Kilgore: unlambda, Ook, m4, Jako
Laurent Schneider: XQuery
Hovik Melikyan: Armenian
Sudarshan HS: Kannada, Hindi
Rusak Maxim: Kylix
Evgeniy Polkin: HQ9+, HQ9++
Wikipedia: Piet
Paulius Maru&scaron;ka: Lithuanian
R Shanmuga Sundaram: Tamil
Shaikh Sonny Aman: Ferite
#x: Belarusian
Anton Balabanov: SPSS
Sascha Wilde: Argh!
Mike Quigley: T-SQL
Andrej Popov: Omnimark
Amy de Buitleir: Irish
Leszek Godlewski: MoHAA Script
Don Higgins: IBM z390 Assembler, High Level Assembler
Eric Bergman: FOCUS
IaRRoVaWo: Galician
Rudolfs Eglitis: Latvian
Richard Leggett: ActionScript 2.0 (Flash 8)
Max Wolf: VVVV
Nanci Naomi Arai: IDL, Fortran 90
Libor Tvrdik: Velocity
Jose Miguel Robles Roman: LOTOS
Hyeonseok Shin: CSS
Glaucio Melo: Io
Daniel Campos: Gambas
Uwe Eichler: LIMS Basic
Robert Follett: Microtik Router OS
Jacob Krebs: LS-DYNA
Donna-n-Doug Finner: Lotus Note Formula Language
Nils Rekow: Nullsoft Software Install Script (NSIS)
Atanas Boev: 6502 Assembler (Apple II)
hiphapis: PHP + GD library
Rodrigo B. de Oliveira: Boo
Dan E. Kelley: Gri
Kasper Fehrend: Lotus Script
Tjibbe Rijpma: PostgreSQL
Andrew Cooke: Malbolge
Ebben Feagan: FreeBASIC
Eyal Ronel: LilyPond
Jonathan R.: AutoHotkey
Alyn Ashworth: Welsh
Jalal Noureddine: Arabic
Lucas Lucas: Blitz Plus
Kalman Zsoltai: Centura
Archville: Toy
G&otilde;si Csaba: Hungarian (fixed)
Alberti Istv&aacute;n: PBASIC
Drake Guan: Pawn
Jorge Monasterio: Esperanto
Brett Sinclair: Alpha Five Xbasic
Neil Croft: Powerbasic
RPG GAME: GML
Tomas Lindquist Olsen: D
Craig Bennett: Databasic, PQN/PROC
Sindri Snaer // Zn0w: Icelandic
Mart: easm
Misgana Ambissa: amharic
David Simpson: Mouse
Andrew Jones: C++/CLI (.NET)
Guy Lonne: xblite
Andreas &Ouml;berg: MAXScript
Laureta Asllani: Albanian
Edmanuel Torres: C++-FLTK
Josef Betancourt: Powershell
Dileep R: Malayalam
Bruce V Chiarelli: Hmong
JP Theberge: wml
Jonathan Bergeron: Google Gadget
Nicolaj Christensen: LOLCODE, VHDL
J&uuml;rgen Jung: Visual Works Smalltalk
Simon James Kearon: Bemba
Silvano Riccio: XSLT
Chris de Almeida: K&amp;R's original Hello World program
Shahram Monshipouri: Persian
Soren Dalsgaard: MML for AXE10
Peter Furlan: Trans
Christoph Salomon: SQL (DB2)
Zoltan Adamek aka Scorchio: Max/MSP
Sam Hart: Assembler for Darwin PPC, Hawaiian
Robert Bird: PPL
Xooxer Xoox: DM
Don Reba: Nemerle
Bruno VARON: CLP
Benjamin Webber: Turing
Enrique Ferreyra: Clarion
pelikoira: CoolBasic
Lars Burgstahler: Chef
acm: OpenGL
Garen Arevian: E
Robert Resetar: ZIM
Matthew L. Fischer: Assembler (Itanium and PA-RISC)
Steve Roper: Mobile Phone
Francisco Cabrita: VSL
Andrew Platt: APC
Chris Thornhill: CYBOL
Paul Gafa: Maltese
Frank Hartmann: PEARL
Normann Nielsen: Powerflex
Arturo Olguin Cruz: Pro*C
James McMurrin: Iptscrae
Martijn Lievaart: BIT
Ingmar Steiner: PRAAT
James Landis: UniComal
J A: ARM assembler
Giovanni Ferranti: GRAMophone
John Camara: Scheme (fixed)
Yiwei Xu: VBScript
Jan Krohn: Latin
Steven Kazoullis: RPL, Filemaker Script
Darren Debono: C for Amiga Anywhere
Juan Pablo Espinosa: Miva Script
Tony G., Keymaker: Befunge
Dave Parker: Flaming Thunder
Lutz Mueller: newLISP
Greg Douglas: GameMonkey Script
Dan Phan: Vietnamese
Dennis Furey: Ursala
Paulo Tanimoto: Tagalog
Henrique Dante de Almeida: C++-gtkmm
Kim Nguyen: CDuce
Al Koury: 4Test
Hubert Bielenia: AviSynth
Dave Carmichael: DCL
Keith Forsythe: PeopleCode
Jonathan Taylor: Action!
Jason Rogers: ICL SCL
Harald Massa: Python 3000
Patrice Blancho: Thue, FALSE
Angus Strong: JADE
Geoff Canyon: J
Angel David Reyes Figueroa: NXC
Daniel Hugenroth: Casio BASIC
Jose Antonio Chacon: ActionScript 3
Olivier Rossiny: Vala
Artur Gajowy: Scilab
Morgan Dell: Rational Rose
Kenneth Vanhoey: Luxembourgish, Lisaac
Eunice Ndungu: Kiswahili, Kikuyu
The Happy Hippy: PICAXE BASIC, Spin
Veselov Ilya: C#
Poon Poon: F#
James Irwin: REBOL
Ronaldo Lachenal: Maximus BBS
Mike Wessler: Quartz Composer
Paulo Lima: Xbase++
Y R U 2 L8: K
Fabian Andres Chara Parra: SQR
Database Consulting Group: D4
Bruno Kenj: ASP.NET
Chase Peeler: XLogo
eNTiDi di Fontana Nicola: MetaPost
Roman Stumm: Smalltalk MT
Diego Charles: Jess
Renan Caldeira: BMC Remedy
Jomi Hubner: Jason
Charles Goodwin: Vexi
Paulo Dionisio Coelho: Kix
Tim Cas: Arena
Charlie Nolan: MOO
Wacek Kusnierczyk: OZ
Nicholas Kingsley: BlitzMax
Tijs van Bakel: Intercal
Za3k: Scheme
Adam Majewski: Maxima
Florian Jenett: Processing
Vlad Albulescu: Cool
James Kingsley: GLBasic
Tom Ragle: BuddyScript
Ben Aceler: Parser
Marcio Automatiza: Assembler (8051)
Robert Gosling: CICS COBOL
Vijaye Raji: MS Small Basic
Yoav Krauz: Byte Syze
Martin Bishop: Modula-3
Pawel Baszuro: Rey
Brian Tiffin: Falcon
Love Gr&ouml;nvall: AMOS
Andrei Bautu: BibTex
Guy Selis: Cach&eacute; Object Script
Nigi: LPC
Makc (and many others): Go
Julio Wittwer: ADVPL
ellvis/Zeroteam: Sinclair Basic
Mark Hagenau: TinyFugue
Eliezer Basner: JCL
aurele: Mythryl
Larry Soule: QR code image
Nathan Christiansen: Cherokee
Francesco Di Filippo: Guile, Squirrel, AngelScript
Gero Zahn: SSI
Nataniel L&oacute;pez: MDM Zinc
Nathapon Vaiyavuthipinyo: Thai, Lao
Claudio Larini: ERRE
Seungho Seo: Node.js
Michael V: CoDScript
Jiyoung In: MySQL FUNCTION, Korean
Mads Ohm Larsen: Elixir
/u/geocar: Hoon, and lots of fixes
Frank Burnham: Clojure
chris s: Elm
Stefan Holm: VBA (Excel and Word), Draco, Scratch, Haxe, TypeScript, make
/u/SagetBob: Scala
Adam Johnston: ACPI Source Language
Daniel Temkin: Velato
Herbert Breunung: Perl 6
Dani&euml;l Minnaar: Julia, Afrikaans
Jon Paris: RPG IV
Ingemar Ragnemalm: CUDA
Isaac: Symsyn
Ilia Mikhnevich: BrightScript
Alan Yeung: CA-Easytrieve Plus
Jeff Tomich: cpl
Pouya Kary: Arendelle
Basix: aheui
Paul Koning: Electrologica X1 and X8
Zachary Smith: Arduino
Donald Fisk: Full Metal Jacket
Erik Svedäng: Carp
Mike Finn: Matrix
Lambert Meertens: ABC
Bacco: Harbour
Rolf Huisman: SPARC Assembly Language
/u/farias0: Portugol
Dave Kilroy: LiveCode
Leon (Yinliang) Wu: SAS
Marco Pontello: Z80 Assembly for CP/M
Akash Issar: Stata
Kate Go: MCSBL
Aleksey Rakov: ELENA
Andreas Bjørn Hassing Nielsen: JavaScript
Andreas Wiegenstein: BSP
Ron Aaron: 8th
Jesús Cuerda: Genie
LegionMammal978: Lojban
Ahmed Abdalla: Noor
Joel Bryan Juliano: Bato
Chris Huryn: Splunk SPL
Lutz Haselof: ConTeXt
Peter Bauer: 4D
Robert Weißenfels: BitZ
John D. Gwinner: React-VR
Fiosaiche: Scots Gaelic
Ilya Savistsky: Kumar
Marco Bambini: Gravity
Rainard Buchmann: TAS Assembler for TR 440
Err Enchagaray: Marmelade
Mathew R. Rumsey: Genero BDL
Chris Gray: MPLAB IDE
Err Enchagaray, Семён Белый, Azumi Tanaka: Loli
Yves Charton: HP-85 assembler
Adam Ridemar: Swedish
Aaron Liu: AsciiDots
