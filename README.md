# Source Code for the Hello World Collection

This is the source code from which the Hello World Collection at http://helloworldcollection.de is built.

Source files for the languages are in `files`. Imgages are in `hellopics`.

The Collection is created with `build.sh`. The script is not universal, you'll have to modify it before it works on your computer.

http://youtu.be/bGGkcCZNHj0 shows how the Collection is built (or rather, how it was built before being hosted on GitLab, so some details have changed since then).

To contribute to the Collection, please e-mail me at info@helloworldcollection.de.

---
*Wolfram Rösler • wolfram@roesler-ac.de • https://gitlab.com/wolframroesler • https://twitter.com/wolframroesler • https://www.linkedin.com/in/wolframroesler/*
